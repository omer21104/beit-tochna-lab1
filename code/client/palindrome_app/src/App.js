import logo from './logo.svg';
import './App.css';
import {useState} from "react";
import service from "./api";

function App() {

    const [palindrome, setPalindromeText] = useState('');
    let [palindromeLength, setPalindromeLength] = useState('');

    const generateNewPalindromeOnClick = () => {
        service.PalindromesService.getPalindrome(palindromeLength)
            .then(palindrome => {
                console.log(palindrome);
                setPalindromeText(palindrome.palindrome);
            })
    }

    const handlePalindromeLengthChange = (e) => {
        const re = /^[0-9\b]+$/;

        if (re.test(e.target.value) && e.target.value <= 40) {
            setPalindromeLength(e.target.value)
            setGenerateButtonState(false);
        }
        else {
            setGenerateButtonState(true);
        }
    }

    const setGenerateButtonState = (state) => {
        document.getElementById('generate_button').hidden = state;
    }

    return (
        <div className={'app'}>
            <span>Enter desired palindrome length: </span>
            <input onChange={handlePalindromeLengthChange} type="text"/>
            <br/>
            <button id={'generate_button'} onClick={generateNewPalindromeOnClick}>Click to generate palindrome</button>
            <div className={'palindrome'}>{palindrome}</div>
        </div>
    );
}

export default App;
