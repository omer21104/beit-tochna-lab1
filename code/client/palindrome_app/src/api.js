import Axios from 'axios'

const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})

//Example of a cross-cutting concern - client api error-handling
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });


class PalindromesService
{
    static getPalindrome(length){
        return $axios
            .get(`palindrome/${length}`)
            .then(response => response.data)
    }
}

const service = {
    PalindromesService
}

export default service
