from django.apps import AppConfig


class PalindromeAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'palindrome_app'
