from django.db import models


class Palindromes(models.Model):
    id = models.AutoField(primary_key=True)
    palindrome = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return '%s' % self.palindrome
