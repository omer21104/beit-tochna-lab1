from django.urls import path
from . import views

urlpatterns = [
    path('', views.generate_random_length_palindrome),
    path('<int:length>/', views.generate_palindrome),
]