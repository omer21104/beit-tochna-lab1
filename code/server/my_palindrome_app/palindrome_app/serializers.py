from rest_framework import serializers
from .models import Palindromes


class PalindromesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Palindromes
        fields = ['palindrome']
