import random
import string

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .models import Palindromes
from .serializers import PalindromesSerializer


def generate_even_palindrome(length):
    first_half = generate_random_chars(int(length / 2))
    second_half = reverse_string(first_half)

    return first_half + second_half


def generate_odd_palindrome(length):
    first_half = generate_random_chars(int(length / 2) + 1)
    middle_char = first_half[len(first_half) - 1]
    second_half = reverse_string(first_half[0:len(first_half) - 1])

    return first_half[:len(first_half) - 1] + middle_char + second_half


def generate_random_chars(num_of_chars):
    chars = ""
    for i in range(num_of_chars):
        chars += random.choice(string.ascii_letters)

    return chars


def reverse_string(str):
    reverse_str = ""
    for i in range(len(str) - 1, -1, -1):
        reverse_str += str[i]

    return reverse_str


def persist_new_palindrome(text):
    new_palindrome = Palindromes(palindrome=text)
    new_palindrome.save()

    return new_palindrome


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def generate_palindrome(request, length):
    if length <= 0:
        length = random.randint(4, 20)

    result = ""

    if length % 2 == 0:
        result = generate_even_palindrome(length)
    else:
        result = generate_odd_palindrome(length)

    new_palindrome = persist_new_palindrome(result)
    serializer = PalindromesSerializer(new_palindrome)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def generate_random_length_palindrome(request):
    # Generate a random length until i figure out how to pass arguments
    length = random.randint(4, 20)
    result = ""

    if length % 2 == 0:
        result = generate_even_palindrome(length)
    else:
        result = generate_odd_palindrome(length)

    return Response(result, status=status.HTTP_200_OK)
