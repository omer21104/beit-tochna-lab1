from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('palindrome/', include('palindrome_app.urls')),
    path('api/palindrome/', include('palindrome_app.urls')),
]
